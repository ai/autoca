
++++++
autoca
++++++


A Certification Authority manager with a very simple API (and an HTTP
web interface).

Suitable for low-security authorities that can be used as a web
service. For a practical example, see http://vpn.autistici.org/
where it is used to provide OpenVPN short-term certificates.
