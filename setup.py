#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name="autoca",
    version="0.3.2",
    description="Automated CA management.",
    author="Ale",
    author_email="ale@incal.net",
    url="http://git.autistici.org/ai/autoca",
    license="MIT",
    packages=find_packages(),
    platforms=["any"],
    install_requires=["pyOpenSSL", "Flask"],
    entry_points={
        "console_scripts": [
            "autoca=autoca.ca_tool:main",
        ],
    },
)
