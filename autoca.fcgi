#!/usr/bin/python
# A minimal RESTful Certification Authority
# by <ale@incal.net>, 2010

import os
import yaml
from autoca import ca
from autoca import ca_app

ca_conf_file = os.path.join(os.path.dirname(__file__), 'ca.yml')


def create_app():
    with open(ca_conf_file, 'r') as fd:
        ca_config = yaml.load(fd)
    ca_instance = ca.CA(ca_config['basedir'],
                        ca_config['ca_subj'],
                        int(ca_config.get('bits', '4096')),
                        ca_config.get('digest', 'sha1'))
    app = ca_app.make_app(ca_instance)


if __name__ == '__main__':
    from flup.server.fcgi import WSGIServer
    WSGIServer(create_app()).run()
