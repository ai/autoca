import functools
import logging
from OpenSSL import crypto
from flask import Blueprint, Flask, abort, redirect, request, make_response, \
    g, current_app
from autoca import ca
from autoca import certutil

ca_app = Blueprint('ca', __name__)
log = logging.getLogger(__name__)


def content_type(ctype):
    """Decorator to correctly return X509 certificates."""
    def _ctype_decorator(fn):
        @functools.wraps(fn)
        def _ctype_wrapper(*args, **kwargs):
            resp = fn(*args, **kwargs)
            if isinstance(resp, basestring):
                resp = make_response(resp, 200)
                resp.headers['Content-Type'] = ctype
            return resp
        return _ctype_wrapper
    return _ctype_decorator


def auth(fn):
    @functools.wraps(fn)
    def _auth_wrapper(*args, **kwargs):
        secret = current_app.config.get('CA_SHARED_SECRET')
        if secret and request.headers.get('X-Shared-Secret') != secret:
            return make_response('Unauthorized', 401)
        return fn(*args, **kwargs)
    return _auth_wrapper


@ca_app.before_request
def set_ca_wrapper():
    g.ca = current_app.ca


@ca_app.route('/ca.pem')
@content_type('application/x-x509-ca-cert')
def get_ca():
    return g.ca.get_ca()


@ca_app.route('/crl.pem')
@content_type('application/x-x509-ca-cert')
def get_crl_pem():
    return g.ca.get_crl(format='pem')


@ca_app.route('/ca.crl')
@content_type('application/x-pkcs7-crl')
def get_crl_der():
    return g.ca.get_crl(format='der')


@ca_app.route('/get/<cn>')
@content_type('application/x-x509-user-cert')
def get_certificate(cn):
    cert = g.ca.get_certificate(cn, raw=True)
    if not cert:
        abort(404)
    return cert


@ca_app.route('/revoke/<cn>', methods=['POST'])
@auth
def revoke(cn):
    g.ca.revoke_certificate(cn)
    return 'ok'


@ca_app.route('/sign', methods=['POST'])
@content_type('application/x-x509-user-cert')
@auth
def sign():
    if not request.form.get('csr'):
        abort(400)
    try:
        csr_data = request.form['csr']
        csr = crypto.load_certificate_request(crypto.FILETYPE_PEM, csr_data)
    except Exception, e:
        log.exception('error decoding CSR data: %s', e)
        abort(400)
    server = (request.form.get('server', 'n') == 'y')
    days = int(request.form.get('days', 7))
    signed_cert = g.ca.sign_certificate(
        csr, days=days, server=server)
    return crypto.dump_certificate(crypto.FILETYPE_PEM, signed_cert)


def make_app(ca_instance, settings={}):
    app = Flask(__name__)
    app.register_blueprint(ca_app)
    if settings:
        app.config.update(settings)
    app.config.from_envvar('APP_CONFIG', silent=True)
    app.ca = ca_instance
    return app


def main():
    import optparse
    parser = optparse.OptionParser()
    parser.add_option('--root', dest='root')
    parser.add_option('-p', '--port', dest='port', type='int', default=4000)
    parser.add_option('--bits', dest='bits', type='int', default=4096)
    parser.add_option('--subject', dest='subject')
    opts, args = parser.parse_args()
    if args:
        parser.error('Too many arguments')
    if not opts.root:
        parser.error('--root is mandatory')
    if not opts.subject:
        parser.error('--subject is mandatory')

    ca_instance = ca.CA(opts.root,
                        certutil.parse_subject(opts.subject),
                        opts.bits)
    app = make_app(ca_instance)
    app.debug = True
    app.run(port=opts.port)


if __name__ == '__main__':
    main()

