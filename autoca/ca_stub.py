import logging
import threading
import urllib
import urllib2
from OpenSSL import crypto

from autoca import certutil

log = logging.getLogger(__name__)


class Error(Exception):
    pass


class CaStub(object):
    """Client stub for a remote CA."""

    def __init__(self, url, secret=None):
        self.url = url.rstrip('/')
        self.secret = secret
        self.ca_pem = None
        self._cache_lock = threading.Lock()

    def _request(self, path, method='GET', args=None, raw=True):
        data = None
        if args:
            if method == 'GET':
                path = '%s?%s' % (path, urllib.urlencode(args))
            else:
                data = urllib.urlencode(args)
        headers = {}
        if self.secret:
            headers['X-Shared-Secret'] = self.secret
        request = urllib2.Request(self.url + path, data, headers)
        try:
            response = urllib2.urlopen(request)
            response_data = response.read()
        except urllib2.URLError, e:
            log.error('error accessing %s: %s', path, e)
            raise Error(str(e))
        if not raw:
            ctype = response.headers['Content-Type']
            if ctype in ('application/x-x509-user-cert',
                         'application/x-x509-ca-cert'):
                return crypto.load_certificate(
                    crypto.FILETYPE_PEM, response_data)
            elif ctype == 'application/x-pkcs7-crl':
                return crypto.load_crl(
                    crypto.FILETYPE_ASN1, response_data)
        return response_data

    def get_ca(self):
        with self._cache_lock:
            value = getattr(self, '_ca_pem', None)
            if not value:
                value = self._request('/ca.pem', raw=True)
                self._ca_pem = value
        return value

    def get_crl(self, format='der'):
        url = (format == 'der') and '/ca.crl' or '/crl.pem'
        return self._request(url, raw=True)

    def get_certificate(self, cn, raw=True):
        try:
            return self._request('/get/%s' % cn, raw=raw)
        except Error:
            return None

    def make_certificate(self, subject_attrs, days=7, server=False):
        pkey = certutil.create_rsa_key_pair()
        csr = certutil.create_cert_request(pkey, **subject_attrs)
        return pkey, self.sign_certificate(csr, days, server)

    def sign_certificate(self, req, days=365, server=False):
        csr_data = crypto.dump_certificate_request(crypto.FILETYPE_PEM, req)
        signed_cert = self._request('/sign', 'POST', 
                                    {'csr': csr_data,
                                     'days': str(days),
                                     'server': (server and 'y' or 'n')},
                                    raw=False)
        return signed_cert

    def revoke_certificate(self, cn):
        self._request('/revoke/%s' % cn, 'POST')

