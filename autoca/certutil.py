from OpenSSL import crypto
import socket

CSR_DIGEST = 'sha1'


def writeto(filename, contents):
    with open(filename, 'w') as fd:
        fd.write(contents)


def readfrom(filename):
    with open(filename, 'r') as fd:
        return fd.read()


def create_rsa_key_pair(bits=1024):
    """Generate a new RSA key pair."""
    pkey = crypto.PKey()
    pkey.generate_key(crypto.TYPE_RSA, bits)
    return pkey


def create_cert_request(pkey, **attrs):
    """Generate a new CSR using the given key."""
    if 'CN' not in attrs:
        attrs['CN'] = socket.gethostname()
    req = crypto.X509Req()
    subj = req.get_subject()
    for key, value in attrs.items():
        setattr(subj, key, value)
    req.set_pubkey(pkey)
    req.sign(pkey, CSR_DIGEST)
    return req


def sign_certificate(req, ca_key, ca_crt, serial_num, days,
                     extensions=None, digest='sha1'):
    cert = crypto.X509()
    cert.set_serial_number(serial_num)
    cert.gmtime_adj_notBefore(0)
    cert.gmtime_adj_notAfter(86400 * days)
    cert.set_issuer(ca_crt.get_subject())
    cert.set_subject(req.get_subject())
    cert.set_pubkey(req.get_pubkey())
    if extensions:
        # certificate version 3, not a typo
        cert.set_version(2)
        cert.add_extensions(extensions)
    cert.sign(ca_key, digest)
    return cert


def parse_subject(subjstr):
    return dict(x.split('=', 1) for x in subjstr.split(','))


class FakeRevoked(object):

    def set_serial(self, serial):
        self.serial = serial


class FakeCRL(object):

    def __init__(self):
        self.entries = set()

    def get_revoked(self):
        return self.entries

    def add_revoked(self, r):
        self.entries.add(r)

    def export(self, cert, key, filetype, days):
        return ''


# Work around missing CRL implementation in PyOpenSSL < 0.11.
if not hasattr(crypto, 'CRL'):
    crypto.Revoked = FakeRevoked
    crypto.CRL = FakeCRL
    crypto.load_crl = lambda x, y: FakeCRL()
