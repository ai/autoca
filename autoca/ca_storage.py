import fcntl
import os
import time

import certutil


class LockedFile(object):

    def __init__(self, path):
        self._path = path

    def __enter__(self):
        try:
            self._fd = open(self._path, 'r+')
        except IOError:
            self._fd = open(self._path, 'w+')
        fcntl.lockf(self._fd, fcntl.LOCK_EX)
        return self._fd

    def __exit__(self, type, value, traceback):
        fcntl.lockf(self._fd, fcntl.LOCK_UN)
        self._fd.close()


class FileStorage(object):

    def __init__(self, root):
        self.root = root
        self.certs_dir = os.path.join(root, 'certs')
        self.key_dir = os.path.join(root, 'private')
        for path in (self.root, self.certs_dir, self.key_dir):
            if not os.path.isdir(path):
                os.mkdir(path, 0700)

        # Special files.
        self.serial_path = os.path.join(root, 'serial')
        self.revoked_path = os.path.join(root, 'revoked')
        self.ca_crt_path = os.path.join(root, 'ca.pem')
        self.ca_key_path = os.path.join(self.key_dir, 'ca.key')
        self.crl_path = os.path.join(root, 'crl.pem')

    def _cert_path(self, cn):
        return os.path.join(self.certs_dir,
                            cn.replace('/', '_') + '.pem')

    def get_ca(self):
        if (os.path.exists(self.ca_crt_path) 
            and os.path.exists(self.ca_key_path)):
            return (certutil.readfrom(self.ca_key_path),
                    certutil.readfrom(self.ca_crt_path))
        else:
            return (None, None)

    def set_ca(self, key_str, cert_str):
        certutil.writeto(self.ca_crt_path, cert_str)
        certutil.writeto(self.ca_key_path, key_str)
        os.chmod(self.ca_key_path, 0400)

    def get_crl(self):
        if os.path.exists(self.crl_path):
            return certutil.readfrom(self.crl_path)

    def set_crl(self, crl_str):
        certutil.writeto(self.crl_path, crl_str)

    def get_certificate(self, cn):
        path = self._cert_path(cn)
        if os.path.exists(path):
            return certutil.readfrom(path)

    def store_certificate(self, cn, cert_data):
        certutil.writeto(self._cert_path(cn), cert_data)

    def delete_certificate(self, cn):
        os.unlink(self._cert_path(cn))

    def get_next_serial(self):
        with LockedFile(self.serial_path) as fd:
            contents = fd.read()
            fd.seek(0)
            if contents:
                fd.truncate()
                serial = int(contents.strip()) + 1
            else:
                serial = int(time.time())
            fd.write('%d\n' % serial)
            return serial

    def get_revoked(self):
        with LockedFile(self.revoked_path) as fd:
            revoked = [map(int, x.strip().split()) for x in fd if x]
        return revoked

    def add_revoked(self, serial):
        with LockedFile(self.revoked_path) as fd:
            fd.seek(0, 2)
            fd.write('%s %i\n' % (serial, time.time()))

