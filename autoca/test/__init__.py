from OpenSSL import crypto
import unittest
import tempfile
import shutil
import subprocess
import os

from autoca import ca


class CaTestBase(unittest.TestCase):

    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()
        self.ca_subject = {'CN': 'Test CA',
                           'O': 'Test Corp.',
                           'L': 'IE'}
        self.ca = ca.CA(self.tmpdir, self.ca_subject, bits=1024)

    def tearDown(self):
        shutil.rmtree(self.tmpdir)

    def _run_openssl(self, cmdline, input_data):
        pipe = subprocess.Popen(['openssl'] + cmdline.split(),
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE)
        output = pipe.communicate(input_data)[0]
        return pipe.returncode, output

