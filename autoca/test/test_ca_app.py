import unittest
import urllib
import StringIO
from OpenSSL import crypto

from autoca import certutil
from autoca import ca_app
from autoca.test import *


class CaAppTest(CaTestBase):

    def setUp(self):
        CaTestBase.setUp(self)
        self.app = ca_app.make_app(self.ca)

    def _request(self, url, method='GET', data=None):
        if '?' not in url:
            url = url + '?'
        path, qs = url.split('?', 1)
        environ = {'wsgi.url_scheme': 'http',
                   'PATH_INFO': path,
                   'QUERY_STRING': qs,
                   'REQUEST_URI': url,
                   'REQUEST_METHOD': method,
                   'SERVER_NAME': 'test',
                   'SERVER_PORT': '80'}
        if data:
            enc_data = urllib.urlencode(data)
            environ['wsgi.input'] = StringIO.StringIO(enc_data)
            environ['CONTENT_TYPE'] = 'application/x-www-form-urlencoded'
            environ['CONTENT_LENGTH'] = str(len(enc_data))
            
        def start_response(status, headers):
            self._response_status = int(status[:3])
            self._response_headers = dict(headers)
        output = ''.join(self.app(environ, start_response))
        return (self._response_status, self._response_headers, output)

    def _sign_cert(self):
        pkey = certutil.create_rsa_key_pair()
        csr = certutil.create_cert_request(pkey, CN='testcn')
        csr_data = crypto.dump_certificate_request(crypto.FILETYPE_PEM, csr)
        status, hdrs, output = self._request(
            '/sign', 'POST', {'csr': csr_data})
        self.assertEquals(200, status)
        self.assertEquals('application/x-x509-user-cert',
                          hdrs['Content-Type'])
        crt = crypto.load_certificate(crypto.FILETYPE_PEM, output)
        return crt

    def test_get_ca_pem(self):
        status, hdrs, output = self._request('/ca.pem')
        self.assertTrue(200, status)
        self.assertEquals('application/x-x509-ca-cert',
                          hdrs['Content-Type'])
        self.assertEquals(self.ca.public_ca_pem, output)

    def test_get_crl_pem(self):
        status, hdrs, output = self._request('/crl.pem')
        self.assertTrue(200, status)
        self.assertEquals('application/x-x509-ca-cert',
                          hdrs['Content-Type'])
        self.assertEquals(self.ca.crl_data_pem, output)

    def test_get_crl_der(self):
        status, hdrs, output = self._request('/ca.crl')
        self.assertTrue(200, status)
        self.assertEquals('application/x-pkcs7-crl',
                          hdrs['Content-Type'])
        self.assertEquals(self.ca.crl_data_der, output)

    def test_sign(self):
        crt = self._sign_cert()
        self.assertTrue(crt is not None)

    def test_sign_bad_csr(self):
        status, hdrs, output = self._request(
            '/sign', 'POST', {'csr': 'bad'})
        self.assertEquals(400, status)

        status, hdrs, output = self._request(
            '/sign', 'POST', {'fuffa': 'true'})
        self.assertEquals(400, status)

    def test_get_certificate(self):
        crt = self._sign_cert()
        crt_data = crypto.dump_certificate(crypto.FILETYPE_PEM, crt)
        status, hdrs, output = self._request('/get/testcn')
        self.assertEquals(200, status)
        self.assertEquals('application/x-x509-user-cert',
                          hdrs['Content-Type'])
        self.assertEquals(crt_data, output)

    def test_get_nonexisting_certificate(self):
        status, hdrs, output = self._request('/get/nonexisting')
        self.assertEquals(404, status)

    def test_revoke(self):
        status, hdrs, output = self._request('/revoke/testcn', 'POST')
        self.assertEquals(200, status)
        self.assertEquals(None, self.ca.get_certificate('testcn'))

    def test_revoke_nonexisting_certificate(self):
        status, hdrs, output = self._request('/revoke/fuffa', 'POST')
        self.assertEquals(200, status)

