from OpenSSL import crypto
import unittest
import urllib2
import os
import mox

from autoca import ca_app
from autoca import ca_stub
from autoca import certutil
from autoca.test import *

TEST_URL = 'https://my.ca.org'


class FakeResponse(object):

    def __init__(self, data, content_type):
        self.data = data
        self.headers = {'Content-Type': content_type}

    def read(self):
        return self.data


def IsUrl(url):
    def _comparator(x):
        return (isinstance(x, urllib2.Request) and 
                x.get_full_url() == (TEST_URL + url))
    return mox.Func(_comparator)


class CaStubTest(mox.MoxTestBase, CaTestBase):

    def setUp(self):
        mox.MoxTestBase.setUp(self)
        CaTestBase.setUp(self)
        self.mox.StubOutWithMock(urllib2, 'urlopen')
        self.stub = ca_stub.CaStub(TEST_URL)

    def tearDown(self):
        CaTestBase.tearDown(self)
        mox.MoxTestBase.tearDown(self)

    def test_get_ca_raw(self):
        urllib2.urlopen(IsUrl('/ca.pem')).AndReturn(
            FakeResponse(self.ca.public_ca_pem,
                         'application/x-x509-ca-cert'))

        self.mox.ReplayAll()
        result = self.stub.get_ca()
        self.assertTrue(isinstance(result, basestring))
        self.assertEquals(self.ca.public_ca_pem, result)

        # Test caching (mox will catch a second urllib call).
        result2 = self.stub.get_ca()
        self.assertEquals(result, result2)

    def test_get_crl(self):
        urllib2.urlopen(IsUrl('/ca.crl')).AndReturn(
            FakeResponse(self.ca.crl_data_der,
                         'application/x-pkcs7-crl'))

        self.mox.ReplayAll()
        result = self.stub.get_crl()
        self.assertTrue(result is not None)

    def test_get_certificate(self):
        pkey = certutil.create_rsa_key_pair()
        csr = certutil.create_cert_request(pkey, CN='testcn')
        cert = self.ca.sign_certificate(csr)
        cert_str = crypto.dump_certificate(crypto.FILETYPE_PEM, cert)

        urllib2.urlopen(IsUrl('/get/testcn')).AndReturn(
            FakeResponse(cert_str, 'application/x-x509-user-cert'))

        self.mox.ReplayAll()
        result = self.stub.get_certificate('testcn', raw=False)
        self.assertEquals('testcn', result.get_subject().CN)

    def test_get_certificate_raw(self):
        pkey = certutil.create_rsa_key_pair()
        csr = certutil.create_cert_request(pkey, CN='testcn')
        cert = self.ca.sign_certificate(csr)
        cert_str = crypto.dump_certificate(crypto.FILETYPE_PEM, cert)

        urllib2.urlopen(IsUrl('/get/testcn')).AndReturn(
            FakeResponse(cert_str, 'application/x-x509-user-cert'))

        self.mox.ReplayAll()
        result2 = self.stub.get_certificate('testcn', raw=True)
        self.assertTrue(isinstance(result2, basestring))
        self.assertEquals(cert_str, result2)

    def test_get_nonexisting_certificate(self):
        urllib2.urlopen(IsUrl('/get/missingcn')).AndRaise(
            urllib2.URLError('not found'))

        self.mox.ReplayAll()
        result = self.stub.get_certificate('missingcn')
        self.assertEquals(None, result)

    def test_make_certificate(self):
        pkey = certutil.create_rsa_key_pair()
        csr = certutil.create_cert_request(pkey, CN='testcn')
        cert = self.ca.sign_certificate(csr)
        cert_str = crypto.dump_certificate(crypto.FILETYPE_PEM, cert)

        urllib2.urlopen(IsUrl('/sign')).AndReturn(
            FakeResponse(cert_str, 'application/x-x509-user-cert'))
        self.mox.ReplayAll()

        key, result = self.stub.make_certificate({'CN': 'testcn'})
        self.assertTrue(key is not None)
        self.assertEquals('testcn', result.get_subject().CN)

    def test_revoke_certificate(self):
        urllib2.urlopen(IsUrl('/revoke/testcn')).AndReturn(
            FakeResponse('ok', 'text/html'))

        self.mox.ReplayAll()
        self.stub.revoke_certificate('testcn')
        
