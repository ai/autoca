from OpenSSL import crypto
import unittest
import os

from autoca import ca
from autoca import certutil
from autoca.test import *


class CaTest(CaTestBase):

    def test_sanity_checks(self):
        self.assertTrue(os.path.exists(self.ca.storage.ca_key_path))
        self.assertTrue(os.path.exists(self.ca.storage.ca_crt_path))

        # Test subject attributes of the CA certificate.
        with open(self.ca.storage.ca_crt_path, 'r') as fd:
            cacrt = crypto.load_certificate(crypto.FILETYPE_PEM, fd.read())
        subj = cacrt.get_subject()
        self.assertEquals('Test CA', subj.CN)
        self.assertEquals('Test Corp.', subj.O)
        self.assertEquals('IE', subj.L)

        # Test fetching a non-existing certificate.
        self.assertEquals(None, self.ca.get_certificate('missing'))

    def test_reinit(self):
        ca2 = ca.CA(self.tmpdir, self.ca_subject, bits=1024)
        self.assertEquals(self.ca.public_ca_pem, ca2.public_ca_pem)

    def test_get_ca(self):
        self.assertEquals(self.ca.public_ca_pem,
                          self.ca.get_ca())

    def test_get_crl_pem(self):
        crl_pem = self.ca.get_crl(format='pem')
        status, output = self._run_openssl(
            'crl -inform PEM -noout -text', crl_pem)
        self.assertEquals(0, status)
        self.assertTrue('No Revoked Certificates.' in output)

    def test_get_crl_der(self):
        crl_pem = self.ca.get_crl(format='der')
        status, output = self._run_openssl(
            'crl -inform DER -noout -text', crl_pem)
        self.assertEquals(0, status)
        self.assertTrue('No Revoked Certificates.' in output)

    def test_get_missing_certificate(self):
        result = self.ca.get_certificate('missing', raw=False)
        self.assertEquals(None, result)
        result = self.ca.get_certificate('missing', raw=True)
        self.assertEquals(None, result)

    def test_get_missing_serial(self):
        result = self.ca.get_serial('missing')
        self.assertEquals(None, result)

    def test_make_certificate(self):
        pkey, crt = self.ca.make_certificate({'CN': 'testcn'})
        self.assertEquals('testcn', crt.get_subject().CN)

    def test_sign_certificate(self):
        pkey = certutil.create_rsa_key_pair()
        request = certutil.create_cert_request(pkey, CN='testcn')
        result = self.ca.sign_certificate(request)
        self.assertTrue(result is not None)
        self.assertEquals('testcn', result.get_subject().CN)

        # Check that the certificate is now stored in the CA db.
        result2 = self.ca.get_certificate('testcn')
        self.assertTrue(result2 is not None)
        
        # Check the serial number.
        serial_no = self.ca.get_serial('testcn')
        self.assertTrue(int(serial_no) > 1)

    def test_sign_certificate_twice(self):
        pkey = certutil.create_rsa_key_pair()
        request = certutil.create_cert_request(pkey, CN='testcn')
        result = self.ca.sign_certificate(request)
        self.assertTrue(result is not None)
        serial_no = int(result.get_serial_number())

        pkey2 = certutil.create_rsa_key_pair()
        request2 = certutil.create_cert_request(pkey2, CN='testcn')
        result2 = self.ca.sign_certificate(request)
        self.assertTrue(result2 is not None)
        serial_no2 = int(result2.get_serial_number())

        # Check that we have the same CN.
        self.assertEquals(result.get_subject().CN,
                          result2.get_subject().CN)

        # Check that the serial numbers are monotonically incrementing.
        self.assertNotEquals(serial_no, serial_no2)
        self.assertTrue(serial_no2 > serial_no)

        # Check that get_certificate() returns the latest cert.
        self.assertEquals(serial_no2,
                          self.ca.get_serial('testcn'))

        # Check that a CRL file has been generated.
        self.assertTrue(os.path.exists(self.ca.storage.crl_path))

    def test_revoke_certificate(self):
        pkey = certutil.create_rsa_key_pair()
        request = certutil.create_cert_request(pkey, CN='testcn')
        result = self.ca.sign_certificate(request)
        self.assertTrue(result is not None)
        self.assertEquals('testcn', result.get_subject().CN)
        serial = result.get_serial_number()

        # Check that the certificate is now stored in the CA db.
        result2 = self.ca.get_certificate('testcn')
        self.assertTrue(result2 is not None)
        
        self.ca.revoke_certificate('testcn')

        result3 = self.ca.get_certificate('testcn')
        self.assertEquals(None, result3)

        # Check that the serial appears in the revocation list.
        revoked = set(x[0] for x in self.ca.storage.get_revoked())
        self.assertTrue(serial in revoked)

        # Try to revoke it twice, expect no errors. Detect unexpected
        # calls to _update_crl and raise an exception.
        def unexpected(x):
            raise Exception('unexpected call')
        self.ca._update_crl = unexpected
        self.ca.revoke_certificate('testcn')

        # Examine the CRL and verify that it contains the serial.
        crl = self.ca.get_crl(format='pem')
        status, output = self._run_openssl(
            'crl -inform PEM -noout -text', crl)
        self.assertEquals(0, status)
        self.assertTrue(str(serial) in output)


if __name__ == '__main__':
    unittest.main()
