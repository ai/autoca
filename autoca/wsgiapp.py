import os
from autoca import ca
from autoca.ca_app import make_app
from autoca import certutil


def create_app():
    config = {
        'CERTIFICATE_BITS': 2048,
        'DATA_DIR': './data',
        'CA_SUBJECT': 'ou=CA',
        'CA_DIGEST': 'sha256',
    }
    # APP_CONFIG *must* be defined.
    execfile(os.getenv('APP_CONFIG'), {}, config)
    ca_instance = ca.CA(config['DATA_DIR'],
                        certutil.parse_subject(config['CA_SUBJECT']),
                        config['CERTIFICATE_BITS'])
    return make_app(ca_instance)

